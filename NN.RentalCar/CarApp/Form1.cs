﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarApp {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            try {
                CarManagementSvc.CarManagementClient client = new CarManagementSvc.CarManagementClient();
                var cars = client.GetCars();

                foreach (var item in cars) {
                    listBox1.Items.Add(item.BrandName); 
                }
            }
            catch (Exception ex) {
                textBox1.Text = ex.Message;
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            try {
                CarManagementSvc.CarManagementClient client = new CarManagementSvc.CarManagementClient();
                CarManagementSvc.CarDto car = new CarManagementSvc.CarDto() {
                    BrandName = "BMW",
                    TransmissionType = CarManagementSvc.TransmissionType.Automatic,
                    TypeName = "320d"                    
                };
                textBox1.Text = client.InsertNewCar(car).ToString();
            }
            catch (Exception ex) {
                textBox1.Text = ex.Message;
            }
        }

        private void button3_Click(object sender, EventArgs e) {
            try {
                CarManagementSvc.CarManagementClient client = new CarManagementSvc.CarManagementClient();
                byte[] buff = client.GetCarPicture(int.Parse(textBox1.Text));
                TypeConverter typeConverter = TypeDescriptor.GetConverter(typeof(Bitmap));
                Bitmap bitmap = typeConverter.ConvertFrom(buff) as Bitmap;

                pictureBox1.Image = bitmap;
            }
            catch (Exception ex) {
                textBox1.Text = ex.Message;
            }
        }
    }
}
