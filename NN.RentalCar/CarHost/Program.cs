﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using CarManagementContracts;
using CarManagementService;
using System.IO;

namespace CarHost {
    class Program { 
        static ServiceHost _carManagementSvcHost;

        static void Main(string[] args) {
            Console.WriteLine("ServiceHost");

            try {
                _carManagementSvcHost = new ServiceHost(typeof(CarManagementSvc));
                _carManagementSvcHost.Open();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("CarManagementSvcHost - Started");
            Console.ReadKey();
        }
    }
}
