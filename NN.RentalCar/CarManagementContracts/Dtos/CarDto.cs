﻿using System.Runtime.Serialization;

namespace CarManagementContracts {
    [DataContract]
    [KnownType(typeof(LuxuryCarDto))]
    [KnownType(typeof(SportCarDto))]
    public class CarDto {
        [DataMember]
        public string BrandName {get; set;}

        [DataMember]
        public string TypeName { get; set; }

        [DataMember]
        public TransmissionType TransmissionType { get; set; }

        [DataMember]
        public int NumberOfDoors{ get; set; }

        [DataMember]
        public int MaxNumberOfPersons{ get; set; }

        [DataMember]
        public int LittersOfLuggage{ get; set; }
    }
}
