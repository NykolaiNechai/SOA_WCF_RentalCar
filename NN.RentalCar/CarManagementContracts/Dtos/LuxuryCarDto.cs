﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CarManagementContracts {
    [DataContract]
    public class LuxuryCarDto : CarDto {
        [DataMember]
        public List<LuxuryItems> LuxuryItemsList { get; set; }
    }

    [DataContract]
    public class LuxuryItems {
        [DataMember]
        public string ItemName { get; set; }

        [DataMember]
        public string ItemDescription { get; set; }
    }
}
