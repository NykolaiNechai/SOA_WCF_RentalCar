﻿using System.Runtime.Serialization;

namespace CarManagementContracts {
    [DataContract]
    public class SportCarDto : CarDto {
        [DataMember]
        public int Horsepower { get; set; }
    }
}
