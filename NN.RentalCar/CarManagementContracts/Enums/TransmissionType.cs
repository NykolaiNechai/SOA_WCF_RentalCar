﻿using System.Runtime.Serialization;

namespace CarManagementContracts {
    [DataContract]
    public enum TransmissionType {
        [EnumMember]
        Manual,

        [EnumMember]
        Automatic
    }
}
