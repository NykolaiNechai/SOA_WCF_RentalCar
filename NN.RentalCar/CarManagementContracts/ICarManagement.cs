﻿using System.Collections.Generic;
using System.ServiceModel;

namespace CarManagementContracts {
    [ServiceContract]
    public interface ICarManagement {
        [OperationContract]
        int InsertNewCar(CarDto car);

        [OperationContract]
        bool RemoveCar(CarDto car);

        [OperationContract]
        void UpdateMilage(CarDto car);

        [OperationContract]
        IList<CarDto> GetCars();

        [OperationContract]
        byte[] GetCarPicture(int carId);
    }
}
