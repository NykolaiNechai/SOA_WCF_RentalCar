﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using CarManagementContracts;
using System.IO;

namespace CarManagementService {
    public class CarManagementSvc : ICarManagement {
        public byte[] GetCarPicture(int carId) {

            Console.WriteLine("GetCarPicture");

            byte[] buff;
            var pathDirectory = Directory.GetCurrentDirectory();
            string pathFile = Path.GetFullPath(Path.Combine(pathDirectory, @"..\..\..\DSC00079.JPG"));

            var fileStream = new FileStream(pathFile, FileMode.Open, FileAccess.Read);
            var binaryReader = new BinaryReader(fileStream);

            buff = binaryReader.ReadBytes((int)fileStream.Length);

            return buff;
        }

        public IList<CarDto> GetCars() {
            Console.WriteLine("List cars");
            var cars = new List<CarDto>();
            cars.Add(new CarDto { BrandName = "Audi", TransmissionType = TransmissionType.Automatic, TypeName = "A4"});
            cars.Add(new CarDto { BrandName = "Volkswagen", TransmissionType = TransmissionType.Automatic, TypeName = "Golf" });
            cars.Add(new SportCarDto { BrandName = "Ferrari", TransmissionType = TransmissionType.Automatic, TypeName = "F1", Horsepower = 600 });
            cars.Add(new LuxuryCarDto { BrandName = "RolsRoyes", TransmissionType = TransmissionType.Automatic, TypeName = "F1", LuxuryItemsList = new List<LuxuryItems> { new LuxuryItems {ItemName="Cc1", ItemDescription = "ClimeteControl" } } });  

            return cars;
        }

        public int InsertNewCar(CarDto car) {
            Console.WriteLine("InsertNewCar: {0}", car.BrandName);

            return 1;
        }

        public bool RemoveCar(CarDto car) {
            Console.WriteLine("RemoveCar: {0}", car.BrandName);

            return true;
        }

        public void UpdateMilage(CarDto car) {
            Console.WriteLine("UpdateMilage: {0}", car.BrandName);
        }
    }
}
