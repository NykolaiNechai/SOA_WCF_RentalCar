﻿create database [CarRentalcaseDB]
go 
use [CarRentalcaseDB]
go
Create table[dbo].[Rental](
RentalId int Identity(1,1) not null,
CustomerId int null,
CarId int null,
PickUpLocation int null,
PickUpDateTime datetime null,
DropOffLocation int null,
DropOffDateTime datetime null,
PaymentStatus char(3) null,
Comments nvarchar(1000) null,
constraint PK_Rental Primary KEY Clustered (RentalId ASC))
go
create table [dbo].[Customer](
CustomerId int Identity(1,1) not null,
CustomerLastName nvarchar (50) null,
CustomerFirstName nvarchar (50) null,
CustomerMiddleName nvarchar (50) null,
CustomerBirthDate datetime null,
constraint PK_Customer primary key clustered (CustomerId ASC))
go