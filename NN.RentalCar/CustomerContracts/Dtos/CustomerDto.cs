﻿using System;
using System.Runtime.Serialization;

namespace CustomerContracts {
    [DataContract]
    public class CustomerDto {
        [DataMember]
        public string CustomerLastName { get; set; }

        [DataMember]
        public string CustomerFirstName { get; set; }

        [DataMember]
        public string CustomerMiddleName { get; set; }

        [DataMember]
        public DateTime CustomerBirthDate { get; set; }
    }
}
