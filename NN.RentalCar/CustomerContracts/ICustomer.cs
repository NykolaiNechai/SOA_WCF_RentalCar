﻿using System.ServiceModel;

namespace CustomerContracts {
    [ServiceContract]
    public interface ICustomer {
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        int RegisterCustomer(CustomerDto customer);
    }
}
