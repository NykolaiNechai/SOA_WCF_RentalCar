﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using CustomerService;

namespace CustomerHost {
    class Program {
        static ServiceHost _customerSvcHost;

        static void Main(string[] args) {
            Console.WriteLine("ServiceHost");

            try {
                _customerSvcHost = new ServiceHost(typeof(CustomerSvc));
                _customerSvcHost.Open();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("CustomerSvcHost - Started");
            Console.ReadKey();
        }
    }
}
