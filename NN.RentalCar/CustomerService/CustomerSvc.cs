﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using CustomerContracts;

namespace CustomerService {
    public class CustomerSvc : ICustomer {
        [OperationBehavior(TransactionAutoComplete = true, TransactionScopeRequired = true)]
        public int RegisterCustomer(CustomerDto customer) {

            using (DataCustomerDataContext ctx = new DataCustomerDataContext()) {
                Customer customerToInsert = new Customer();
                customerToInsert.CustomerLastName = customer.CustomerLastName;  
                customerToInsert.CustomerFirstName = customer.CustomerFirstName;
                customerToInsert.CustomerMiddleName = customer.CustomerMiddleName;
                customerToInsert.CustomerBirthDate = customer.CustomerBirthDate;

                ctx.Customers.InsertOnSubmit(customerToInsert);
                ctx.SubmitChanges();

                return customerToInsert.CustomerId;
            }
        }
    }
}
