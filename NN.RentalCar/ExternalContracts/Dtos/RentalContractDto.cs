﻿using CustomerContracts;
using RentalContracts;
using System.Runtime.Serialization;

namespace ExternalContracts {
    [DataContract]
    public class RentalContractDto { 
        public string Company { get; set; }

        [DataMember]
        public int CompanyReferenceId { get; set; }

        [DataMember]
        public RentalRegistrationDto RentalRegistration { get; set; }

        [DataMember]
        public CustomerDto Customer { get; set; }
    }
}
