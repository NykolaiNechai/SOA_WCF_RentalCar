﻿using System.ServiceModel;

namespace ExternalContracts {
    [ServiceContract]
    public interface IExternal {
        [OperationContract]
        void SubmitRentalContract(RentalContractDto rentalContract);
    }
}
