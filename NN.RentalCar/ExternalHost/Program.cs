﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using ExternalService;

namespace ExternalHost {
    class Program {        
        static ServiceHost _externalSvcHost;

        static void Main(string[] args) {
            Console.WriteLine("ServiceHost");

            try {
                _externalSvcHost = new ServiceHost(typeof(ExternalSvc));
                _externalSvcHost.Open();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("ExternalSvcHost - Started");
            Console.ReadKey();
        }
    }
}
