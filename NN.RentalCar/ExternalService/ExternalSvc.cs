﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Transactions;
using CustomerContracts;
using RentalContracts;
using ExternalContracts;

namespace ExternalService {
    public class ExternalSvc : IExternal {
        public void SubmitRentalContract(RentalContractDto rentalContract) {

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                NetNamedPipeBinding netNamedPipeBinding = new NetNamedPipeBinding();
                netNamedPipeBinding.TransactionFlow = true;
                
                ICustomer customerServiceChanel = ChannelFactory<ICustomer>.CreateChannel(netNamedPipeBinding, new EndpointAddress("net.pipe://localhost/customersvc"));

                var customerId = customerServiceChanel.RegisterCustomer(rentalContract.Customer);

                IRental rentalServiceChanel = ChannelFactory<IRental>.CreateChannel(netNamedPipeBinding, new EndpointAddress("net.pipe://localhost/rentalsvc"));

                rentalServiceChanel.RegisterCarRental(rentalContract.RentalRegistration);

                scope.Complete();
            }

            throw new NotImplementedException();
        }
    }
}
