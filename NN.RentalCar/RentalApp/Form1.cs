﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentalApp {
    public partial class Form1 : Form {
        private RentalProxy _rentalProxy = new RentalProxy();

        public Form1() {
            InitializeComponent();


        }

        private void button1_Click(object sender, EventArgs e) {
            try {
                var rentalRegistration = new RentalContracts.RentalRegistrationDto();
                rentalRegistration.CustomerId = 1;
                rentalRegistration.CarId = 1001;
                rentalRegistration.DropOffLocation = 1201;
                rentalRegistration.DropOffDateTime = DateTime.Now;

                rentalRegistration.PickUpLocation = 1207;
                rentalRegistration.PickUpDateTime = DateTime.Now;

                _rentalProxy.RegisterCarRental(rentalRegistration);
            }
            catch (FaultException<RentalContracts.RentalRegisterFault> rentalRegisterFault) {
                MessageBox.Show("RentalRegisterFault" + rentalRegisterFault.Message);
            }

            catch (FaultException faultException) {
                MessageBox.Show("FaultException" + faultException.Message);
            }

            catch (EndpointNotFoundException endpointNotFoundException) {
                MessageBox.Show("EndpointNotFoundException" + endpointNotFoundException.Message);
            }

            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            try {
                _rentalProxy.RegistercarRentalAsPayed(111111);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
