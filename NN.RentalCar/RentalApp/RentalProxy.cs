﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using RentalContracts;

namespace RentalApp {
    public class RentalProxy : ClientBase<RentalContracts.IRental>, IRental {

        public RentalProxy() : base ("RentalSvcEndpoint"){

        }

        public string RegisterCarRental(RentalRegistrationDto rentalRegistration) {

            return Channel.RegisterCarRental(rentalRegistration);
        }

        public void RegistercarRentalAsPayed(int rentalId) {

            Channel.RegistercarRentalAsPayed(rentalId);
        }

        public void StartCarRental(int rentalId, int locationId) {

            Channel.StartCarRental(rentalId, locationId);
        }

        public void StopCarrental(int rentalId, int locationId) {

            Channel.StopCarrental(rentalId, locationId);
        }

        public RentalRegistrationDto GetRentalRegistration(int rentalId) {

            return Channel.GetRentalRegistration(rentalId);
        }
    }
}
