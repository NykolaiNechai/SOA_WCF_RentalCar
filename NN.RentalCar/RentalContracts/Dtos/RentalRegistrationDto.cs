﻿using System;
using System.Runtime.Serialization;

namespace RentalContracts {
    [DataContract]
    public class RentalRegistrationDto {
        [DataMember]
        public int CustomerId { get; set; }

        [DataMember]
        public int CarId { get; set; }

        [DataMember]
        public int PickUpLocation { get; set; }

        [DataMember]
        public DateTime PickUpDateTime { get; set; }

        [DataMember]
        public int DropOffLocation { get; set; }

        [DataMember]
        public DateTime DropOffDateTime { get; set; }

        [DataMember]
        public PaymentStatus PaymentStatus { get; set; }

        [DataMember]
        public string Comments { get; set; }
    }
}
