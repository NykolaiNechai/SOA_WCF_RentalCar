﻿using System.Runtime.Serialization;

namespace RentalContracts {
    [DataContract]
    public enum IncludedInsurance {
        [EnumMember]
        Liability = 1,

        [EnumMember]
        Fire = 2,

        [EnumMember]
        TeftProtection = 4,

        [EnumMember]
        All = 1 + 2 + 4
    }
}
