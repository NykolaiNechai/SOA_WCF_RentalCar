﻿using System.Runtime.Serialization;

namespace RentalContracts {
    [DataContract]
    public enum PaymentStatus {
        [EnumMember(Value ="PUV")]
        PaidUpFrontByVoucher,

        [EnumMember(Value = "PUC")]
        PaidUpFrontByCreditCard,

        [EnumMember(Value = "TPP")]
        ToBePaidAtPickUp,

        [EnumMember(Value = "INV")]
        ToBepaidByInvoice
    }
}
