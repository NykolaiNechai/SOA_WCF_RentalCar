﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace RentalContracts {
    [ServiceContract]
    public interface IRental {
        [OperationContract]
        [FaultContract(typeof(RentalRegisterFault))]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string RegisterCarRental(RentalRegistrationDto rentalRegistration);

        [OperationContract]
        [FaultContract(typeof(RentalRegisterFault))]
        void RegistercarRentalAsPayed(int rentalId);

        [OperationContract]
        [FaultContract(typeof(RentalRegisterFault))]
        void StartCarRental(int rentalId, int locationId);

        [OperationContract]
        [FaultContract(typeof(RentalRegisterFault))]
        void StopCarrental(int rentalId, int locationId);

        [OperationContract]
        [FaultContract(typeof(RentalRegisterFault))]
        RentalRegistrationDto GetRentalRegistration(int rentalId);
    }
}
