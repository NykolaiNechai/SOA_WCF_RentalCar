﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RentalContracts {
    [DataContract(Name = "RentalRegisterFault", Namespace = "FaultContracts/RentalRegisterFault")]
    public class RentalRegisterFault {

        [DataMember]
        public string FaultDescription { get; set; }

        [DataMember]
        public int FaultId { get; set; }
    }
}
