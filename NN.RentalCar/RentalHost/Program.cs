﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using RentalService;

namespace RentalHost {
    class Program {
        static ServiceHost _rentalSvcHost;

        static void Main(string[] args) {
            Console.WriteLine("ServiceHost");

            try {
                _rentalSvcHost = new ServiceHost(typeof(RentalSvc));
                _rentalSvcHost.Open();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("RentalSvcHost - Started");
            Console.ReadKey();
        }
    }
}
