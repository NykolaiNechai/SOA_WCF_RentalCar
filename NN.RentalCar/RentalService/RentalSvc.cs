﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using RentalContracts;
using System.Security.Principal;

namespace RentalService {
    public class RentalSvc : IRental {
        public RentalRegistrationDto GetRentalRegistration(int rentalId) {
            throw new NotImplementedException();
        }

        [OperationBehavior(TransactionAutoComplete = true, TransactionScopeRequired = true)]
        public string RegisterCarRental(RentalRegistrationDto rentalRegistration) {
            Console.WriteLine("RegisterCarRental");

            if (rentalRegistration == null) {
                var fault = new RentalRegisterFault();
                fault.FaultId = 1;
                fault.FaultDescription = "Input is not valid, got null value";

                throw new FaultException<RentalRegisterFault>(fault, "");
            }

            try {
                using (DataRentalDataContext ctx = new DataRentalDataContext()) {
                    Rental rentalToInsert = new Rental();
                    rentalToInsert.CustomerId = rentalRegistration.CustomerId;
                    rentalToInsert.CarId = rentalRegistration.CarId;
                    rentalToInsert.Comments = rentalRegistration.Comments;
                    rentalToInsert.CustomerId = rentalRegistration.CustomerId;

                    ctx.Rentals.InsertOnSubmit(rentalToInsert);
                    ctx.SubmitChanges();

                    return "OK";
                }
            }
            catch (Exception ex) {
                var fault = new RentalRegisterFault();
                fault.FaultId = 123;
                fault.FaultDescription = string.Concat("Ann error occurred while inserting the registration", ex.Message);

                throw new FaultException<RentalRegisterFault>(fault, "");
            }            
        }

        [OperationBehavior(Impersonation = ImpersonationOption.Required)]
        public void RegistercarRentalAsPayed(int rentalId) {
            Console.WriteLine("RegisterCarRentalAsPayed " + rentalId);
            Console.WriteLine("WindowsIdentity : {0} ", WindowsIdentity.GetCurrent().Name);
        }

        public void StartCarRental(int rentalId, int locationId) {
            throw new NotImplementedException();
        }

        public void StopCarrental(int rentalId, int locationId) {
            throw new NotImplementedException();
        }
    }
}
